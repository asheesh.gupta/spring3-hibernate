#!/usr/bin/env groovy
def call() {
   // checkout scm
    codeStability()
    codeQuality()
    unitTest()
    codeCoverage()
    dockerFileLinting()
    imageScanning()
}

def codeStability() {
    stage('codeStability') {
    try {   
        sh "cd spring3hibernate ; mvn clean compile"
        echo "Compile successful"
    }
    catch(Exception e) {
        echo "Validation Check Fails"
        throw e 
        }
    }
}

def codeQuality() {
    stage('codeQuality') {
    try {
        sh "cd spring3hibernate ; mvn checkstyle:checkstyle "
        echo "Code-Quality-Checked Successfully"            
    }
    catch(Exception e) { 
        echo "Code Quality is not maintained"
        throw e
        }
    
    try {
        sh "cd spring3hibernate ; mvn pmd:pmd "
        echo "Code-Quality-Checked Successfully of pmd plugin"            
    }
    catch(Exception e) { 
        echo "Code Quality is not maintained"
        throw e
        }
    
    try {
        sh "cd spring3hibernate ; mvn findbugs:findbugs"
        echo "Code-Quality-Checked Successfully of findbugs plugin"            
    }
    catch(Exception e) { 
        echo "Code Quality is not maintained"
        throw e
        }

}
}

def unitTest() {
    try {
        sh "cd spring3hibernate ; mvn test"
        echo "Unit Test checking"            
    }
    catch(Exception e) { 
        echo "Code Quality is not maintained"
        throw e
        }
    }


def codeCoverage() {
    try {
        sh "cd spring3hibernate ; mvn cobertura:cobertura"
        echo "Unit Test checking"            
    }
    catch(Exception e) { 
        echo "Code Quality is not maintained"
        throw e
        }
    }
